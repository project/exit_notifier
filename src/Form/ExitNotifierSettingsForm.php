<?php

namespace Drupal\exit_notifier\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure exit_notifier settings for this site.
 */
class ExitNotifierSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'exit_notifier_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = \Drupal::config('exit_notifier.settings');
    return [
      'title' => $default_config->get('default.title'),
      'body' => $default_config->get('default.body'),
      'selector' => $default_config->get('default.selector'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'exit_notifier.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('exit_notifier.settings');

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('title'),
      '#required' => TRUE,
    ];

    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config->get('body'),
      '#required' => TRUE,
    ];

    $form['selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Selector (jQuery)'),
      '#default_value' => $config->get('selector'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('exit_notifier.settings');
    $config->set('title', $form_state->getValue('title'))
      ->set('body', $form_state->getValue('body'))
      ->set('selector', $form_state->getValue('selector'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
