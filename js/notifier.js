/**
 * @file notifier.js
 */
(function ($, Drupal, drupalSettings) {
  "use strict";
  var self = null;
  Drupal.behaviors.exit_notifier = {
    settings: null,
    attach: function (context, settings) {
      self = this;
      self.settings = drupalSettings;
      var selector = self.settings.exitNotifier.notifier.selector;
      $(selector).on('click', this.leave);
    },
    leave: function (event) {
      // event.preventDefault();
      var title = self.settings.exitNotifier.notifier.title;
      var body = self.settings.exitNotifier.notifier.body;
      var host = self.settings.exitNotifier.notifier.host;
      var gototext = self.settings.exitNotifier.notifier.gototext;
      var canceltext = self.settings.exitNotifier.notifier.canceltext;
      if(title != '' && body != '') {
        var urlsnippet = event.currentTarget.href;
        if(urlsnippet.indexOf(host) == -1) {
          if (event.currentTarget.href.length > 40) {
            urlsnippet = event.currentTarget.href.substr(0, 40) + "...";
          }

          var goTo = gototext + ' ' + urlsnippet;

          if($("#dialog-exit-notifier").length > 0) {
            $("#dialog-exit-notifier").remove();
          }

          var width = $( window ).width();

          if(width > 600) {
            width = 600;
          } else {
            width = width - 40;
          }

          $("body").append( $('<div id="dialog-exit-notifier" title="' + title + '"><div class="dialog-body">' + body + '</div></div>') );
          $("#dialog-exit-notifier").dialog({
            resizable: true,
            height: "auto",
            width: width,
            modal: true,
            buttons: [
              {
                text: goTo,
                click:function() {
                  window.open(event.currentTarget.href, 'External URL');
                  $( this ).dialog( "close" );
                }
              },
              {
                text: canceltext,
                click:function() {
                  $( this ).dialog( "close" );
                }
              }
            ]
          });
          return false;
        }
      }
      return true;
    }
  }
})(jQuery, Drupal, drupalSettings);
