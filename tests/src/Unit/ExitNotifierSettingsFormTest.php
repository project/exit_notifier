<?php

namespace Drupal\Tests\exit_notifier\Unit;

use \Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\exit_notifier\IceCream
 * @group exit_notifier_settings
 */
class ExitNotifierSettingsFormTest extends UnitTestCase {

  /**
   * @dataProvider validDataList
   * @param $title
   * @param $body
   * @param $query
   */
  public function testFormValidation($title, $body, $query) {

    $form = $this->formBuilder->getForm('Drupal\exit_notifier\Form\ExitNotifierSettingsForm');
    $form_state = (new FormState())
      ->setValue('title', $title)
      ->setValue('body', $body)
      ->setValue('query', $query);

    $form_validator = $this->getMockBuilder('Drupal\Core\Form\FormValidator')
      ->setConstructorArgs(
        [
          new RequestStack(),
          $this->getStringTranslationStub(),
          $this->csrfToken, $this->logger,
          $this->getMock('Drupal\Core\Form\FormErrorHandlerInterface')
        ])
      ->setMethods(NULL)
      ->getMock();
    $form_validator->validateForm('exit_notifier_admin_settings', $form, $form_state);
    $this->assertEmpty($form_state->hasAnyErrors());
  }

  /**
   * Data provider of valid email domains we want to accept.
   * @return array
   */
  public static function validDataList() {
    return [
      ['Title', '<p>Body</p>', 'a'],
    ];
  }

}
